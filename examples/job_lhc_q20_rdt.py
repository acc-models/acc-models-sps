from cpymad.madx import Madx
mad=Madx()

mad.call("sps/sps.seq")
mad.call("sps/strengths/lhc_q20.str")

mad.beam()
mad.use("sps")

tw=mad.twiss()

"pip install pyoptics"
from pyoptics import optics
from matplotlib.pyplot import *
import numpy as np

t=optics(tw)

mad.globals["qx0"]=20.20
mad.call("sps/toolkit/match_tune.madx")

tw=mad.twiss()
t=optics(tw)
plot(t.s,np.cumsum(t.drvterm(3,0)*t.k2l),label="nominal")
t.k2l[t//"lsf..20"]=0
plot(t.s,np.cumsum(t.drvterm(3,0)*t.k2l),label="shunted")
ylabel(r"$\int_0^s e^{2\pi i (3 \mu_x(s'))} \beta_x(s')^{\frac{3}{2}}k_2(s') ds'$")
xlabel("s [m]")
grid()
legend()
title(f'Q_x {mad.globals["qx0"]}')
tight_layout()
savefig("rdt30_20.20.png")


